-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 31, 2022 at 06:39 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `st_waterbilling`
--

-- --------------------------------------------------------

--
-- Table structure for table `billed_household`
--

CREATE TABLE `billed_household` (
  `id` int(11) NOT NULL,
  `household_id` int(11) NOT NULL,
  `del_status` int(11) NOT NULL,
  `paid_status` int(11) NOT NULL,
  `bill_id` int(11) NOT NULL,
  `date_created` varchar(255) NOT NULL,
  `amount_due` varchar(255) NOT NULL,
  `current_reading` int(11) DEFAULT NULL,
  `previous_reading` int(11) DEFAULT NULL,
  `advance_payment` decimal(11,2) NOT NULL,
  `paid_amount` decimal(11,2) NOT NULL,
  `month` varchar(255) NOT NULL,
  `year` varchar(255) NOT NULL,
  `day` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `billed_household`
--

INSERT INTO `billed_household` (`id`, `household_id`, `del_status`, `paid_status`, `bill_id`, `date_created`, `amount_due`, `current_reading`, `previous_reading`, `advance_payment`, `paid_amount`, `month`, `year`, `day`) VALUES
(3, 2, 0, 1, 4, '', '1681.56', 200, 100, '0.00', '2000.00', '10', '2022', '31'),
(4, 2, 0, 1, 5, '', '1476.06', 300, 200, '42.38', '1200.00', '10', '2022', '31');

-- --------------------------------------------------------

--
-- Table structure for table `bill_costs`
--

CREATE TABLE `bill_costs` (
  `id` int(11) NOT NULL,
  `bill_id` int(11) NOT NULL,
  `settings_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bill_costs`
--

INSERT INTO `bill_costs` (`id`, `bill_id`, `settings_id`) VALUES
(5, 4, 1),
(6, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `bill_info`
--

CREATE TABLE `bill_info` (
  `id` int(11) NOT NULL,
  `bill_no` varchar(255) NOT NULL,
  `del_status` int(11) NOT NULL,
  `mainWaterprevious` varchar(255) NOT NULL,
  `mainWatercurrent` varchar(255) NOT NULL,
  `mainElecprevious` varchar(255) NOT NULL,
  `mainEleccurrent` varchar(255) NOT NULL,
  `electric_bill` varchar(255) NOT NULL,
  `kw` varchar(255) NOT NULL,
  `total_consumption` varchar(255) NOT NULL,
  `total_kw_consumed` varchar(255) NOT NULL,
  `price_kw` varchar(255) NOT NULL,
  `total_cost_of_water` varchar(255) NOT NULL,
  `total_electric_bill_peso` varchar(255) NOT NULL,
  `bill_coverage` varchar(255) NOT NULL,
  `present_reading_date` varchar(255) NOT NULL,
  `previous_reading_date` varchar(255) NOT NULL,
  `rateCubic` varchar(255) NOT NULL,
  `mcwd` varchar(255) NOT NULL,
  `due_date` varchar(255) NOT NULL,
  `date_created` varchar(255) NOT NULL,
  `system_loss` int(11) NOT NULL,
  `loss_rate` int(11) NOT NULL,
  `number_of_household` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bill_info`
--

INSERT INTO `bill_info` (`id`, `bill_no`, `del_status`, `mainWaterprevious`, `mainWatercurrent`, `mainElecprevious`, `mainEleccurrent`, `electric_bill`, `kw`, `total_consumption`, `total_kw_consumed`, `price_kw`, `total_cost_of_water`, `total_electric_bill_peso`, `bill_coverage`, `present_reading_date`, `previous_reading_date`, `rateCubic`, `mcwd`, `due_date`, `date_created`, `system_loss`, `loss_rate`, `number_of_household`) VALUES
(4, 'W2022-1000', 0, '800', '1000', '800', '1000', '12', '12', '200', '200', '1', '1681.56', '200', '2022-10-31 - 2022-10-29', '2022-10-30', '2022-10-29', '8.4078', '1471.56', '11/10/2022', '', 100, 100, 1),
(5, 'W2022-1000', 0, '33', '22', '22', '11', '11', '22', '-11', '-11', '0.5', '1476.06', '-5.5', '2022-10-26 - 2022-10-30', '2022-10-21', '2022-10-16', '-134.18727272727273', '1471.56', '11/11/2022', '', -111, -111, 1);

-- --------------------------------------------------------

--
-- Table structure for table `households`
--

CREATE TABLE `households` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `mname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `lot` varchar(255) NOT NULL,
  `block` varchar(255) NOT NULL,
  `phase` varchar(255) NOT NULL,
  `meter_num` varchar(255) NOT NULL,
  `stub_num` varchar(255) NOT NULL,
  `subdivision` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `date_created` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `households`
--

INSERT INTO `households` (`id`, `name`, `mname`, `lname`, `lot`, `block`, `phase`, `meter_num`, `stub_num`, `subdivision`, `phone_number`, `date_created`) VALUES
(2, 'Gerald', 'Magno', 'Serenio', '2', '1', '3', '1', '4', '4', '1', '');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `expense_name` varchar(255) NOT NULL,
  `del_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `amount`, `expense_name`, `del_status`) VALUES
(1, 10, 'Expense', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`) VALUES
(1, 'admin', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `billed_household`
--
ALTER TABLE `billed_household`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bill_costs`
--
ALTER TABLE `bill_costs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bill_info`
--
ALTER TABLE `bill_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `households`
--
ALTER TABLE `households`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `billed_household`
--
ALTER TABLE `billed_household`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `bill_costs`
--
ALTER TABLE `bill_costs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `bill_info`
--
ALTER TABLE `bill_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `households`
--
ALTER TABLE `households`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
