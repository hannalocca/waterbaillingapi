<?php 
class SettingsModel extends CI_Model
{
	public function __construct()
    {
        parent::__construct();
	}
	
	public function getAll(){
		return $this->db->query("SELECT * FROM settings")->result_array();
	}

	public function getLists(){
		return $this->db->query("SELECT * FROM settings WHERE del_status = 0")->result_array();
	}

	public function save($data){
		$this->db->insert("settings",$data);
	}

	public function delete($id){
		$where = array('id' => $id);
		$data = array('del_status' => "1");
		$this->db->update("settings",$data, $where);
	}

	public function login ($user,$pass) {
		return $this->db->query("SELECT * FROM `users` WHERE username = '$user' AND password = '$pass'")->result_array();
	}
}
?>