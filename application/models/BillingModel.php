<?php 
class BillingModel extends CI_Model
{
	public function __construct()
    {
        parent::__construct();
	}
	public function get($id){
		return $this->db->query("SELECT a.*,group_concat(c.expense_name) as expense_name, group_concat(c.amount) as amount
		FROM bill_info a INNER JOIN bill_costs as b on a.id=b.bill_id
		LEFT JOIN settings c ON c.id = b.settings_id WHERE a.id = $id
		GROUP BY a.id")->result_array();
	}

	public function getAll(){
		return $this->db->query("SELECT *,CONCAT(bill_no,'',id) as bill_nos,id as action, id as value,CONCAT(bill_no,'',id) as label FROM bill_info WHERE del_status = 0")->result_array();
	}

	public function save($data){
			$expense_lists = $data['expense_lists'];
			unset($data['expense_lists']);
			
			$this->db->insert("bill_info",$data);
			$insert_id = $this->db->insert_id();
			foreach(json_decode($expense_lists) as $val){
				$arr = array("bill_id" => $insert_id, "settings_id" => $val->id);
				$this->db->insert("bill_costs",$arr);
			}
	}

	public function updateSystemloss($id,$system_loss,$loss_rate,$noOfhousehold){
		
		$where = array('id' => $id);
		$data = array('system_loss'=> $system_loss, 'loss_rate' => $loss_rate, 'number_of_household' => $noOfhousehold);
		$this->db->update("bill_info",$data, $where);
	}

	public function saveBilled($id, $cur, $prev, $amount_due, $bill_id) {
		$data = array('household_id' => $id, 'current_reading' => $cur, 'previous_reading' => $prev, 'amount_due' => $amount_due, 'bill_id' => $bill_id);
		$this->db->insert('billed_household', $data);
	}

	public function checkmeterifExist($meter){
		return $this->db->query("SELECT meter_num FROM households WHERE meter_num = '$meter'")->result_array();
	}

	public function getAllBillbyHousehold($id){
		return $this->db->query("SELECT c.amount_due, c.id, CONCAT(b.bill_no,'',b.id) as bill_nos, b.bill_coverage, CONCAT(a.name, ' ', a.mname,' ', a.lname ) as _name FROM billed_household c
								LEFT JOIN households a ON c.household_id = a.id
								LEFT JOIN bill_info b ON b.id = c.bill_id WHERE c.paid_status = 0 AND c.household_id = $id AND b.del_status = 0")->result_array();
	}

	public function getHouseholdBill($id){
		return $this->db->query("SELECT CONCAT(c.bill_no,'',c.id) as bill_nos, a.*,b.*,c.*, GROUP_CONCAT(e.expense_name) as expense_name, GROUP_CONCAT(e.amount) as amount, 
								(SELECT sum(advance_payment) FROM billed_household WHERE household_id = a.household_id) as advance FROM  billed_household a
								LEFT JOIN households b ON b.id = a.household_id
								LEFT JOIN bill_info c ON c.id = a.bill_id
								LEFT JOIN bill_costs d ON d.bill_id = c.id
								INNER JOIN settings e ON e.id = d.settings_id WHERE a.id = $id")->result_array();
	}

	public function updateHouseholdBill($data){
			$household_id = array('household_id' => $data['household_id']);
			unset($data['household_id']);
			$this->db->update("billed_household",array('advance_payment' => 0), $household_id);
			$where = array('id' => $data['id']);
			$this->db->update("billed_household",$data, $where);
	}

	public function getPaidbillbyhousehold($id){
		return $this->db->query("SELECT a.id as billed_iddd, a.*, b.*, CONCAT(b.bill_no, '', b.id) as bill_nos  FROM billed_household a LEFT JOIN bill_info b ON a.bill_id = b.id WHERE a.paid_status = 1 AND a.household_id = $id AND a.del_status = 0")->result_array();
	}
	public function getUnpaidbillbyhousehold($id){
		return $this->db->query("SELECT a.id as billed_iddd, a.*, b.*, CONCAT(b.bill_no, '', b.id) as bill_nos  FROM billed_household a LEFT JOIN bill_info b ON a.bill_id = b.id WHERE paid_status = 0 AND a.household_id = $id AND a.del_status = 0")->result_array();
	}

	public function getBillReport($id){
		$bill =  $this->db->query("SELECT c.*,a.*, b.*, CONCAT(c.bill_no, '',c.id) as bill_nos, c.bill_coverage,
		(SELECT sum(advance_payment) FROM billed_household WHERE household_id = a.household_id) as advance FROM billed_household a 
						LEFT JOIN households b ON b.id = a.household_id 
						LEFT JOIN bill_info c ON c.id = a.bill_id
						WHERE a.bill_id = $id")->result_array();
		$settings = $this->db->query("SELECT a.*, b.* FROM bill_costs a LEFT JOIN settings b ON b.id = a.settings_id WHERE bill_id = $id")->result_array();				
		
		$result = array();
		array_push($result, $bill);
		$result['costs'] = $settings;
		return $result;
	}

	public function reportPaidStatusHousehold ($bill_id, $status) {
		return $this->db->query("SELECT c.paid_amount,c.amount_due, c.id, CONCAT(b.bill_no,'',b.id) as bill_nos, b.bill_coverage, CONCAT(a.name, ' ', a.mname,' ', a.lname ) as _name FROM billed_household c
		LEFT JOIN households a ON c.household_id = a.id
		LEFT JOIN bill_info b ON b.id = c.bill_id WHERE c.paid_status = $status AND c.bill_id = $bill_id")->result_array();
	}

	public function reportCollectionBymonth($bill_id, $month, $year){
		return $this->db->query("SELECT c.paid_amount, c.amount_due, c.id, CONCAT(b.bill_no,'',b.id) as bill_nos, b.bill_coverage, CONCAT(a.name, ' ', a.mname,' ', a.lname ) as _name FROM billed_household c
		LEFT JOIN households a ON c.household_id = a.id
		LEFT JOIN bill_info b ON b.id = c.bill_id WHERE c.paid_status = 1 AND c.month = $month AND c.year = $year AND c.bill_id = $bill_id")->result_array();
	}

	public function reportCollectionByDay($bill_id, $month, $year, $day){
		$result = $this->db->query("SELECT c.amount_due, c.paid_amount, c.id, CONCAT(b.bill_no,'',b.id) as bill_nos, b.bill_coverage, CONCAT(a.name, ' ', a.mname,' ', a.lname ) as _name FROM billed_household c
		LEFT JOIN households a ON c.household_id = a.id
		LEFT JOIN bill_info b ON b.id = c.bill_id WHERE c.paid_status = 1 AND c.month = $month AND c.day = $day AND c.year = $year")->result_array();
	
		return $result;	
}

	public function delete($id){
		$where = array('id' => $id);
		$data = array('del_status' => "1");
		$this->db->update("bill_info",$data, $where);
	}
}
?>