<?php 
class HouseholdModel extends CI_Model
{
	public function __construct()
    {
        parent::__construct();
	}
	public function get($id){
		return $this->db->query("SELECT * FROM households WHERE id = $id")->result_array();
	}

	public function getAll(){
		return $this->db->query("SELECT id as action, CONCAT(name,' ',mname,' ',lname) as _name,lot,block,phase,meter_num,stub_num, CONCAT(name,' ',mname,' ',lname) as label, id as value FROM households")->result_array();
	}

	public function save($data){
		if($data['id'] == ''){
			unset($data['meterflg']);
			$this->db->insert("households",$data);
		} else {
			$where = array('id' => $data['id']);
			unset($data['meterflg']);
			$this->db->update("households",$data, $where);
		}
	}

	public function delete($id){
		$this->db->where('id', $id);
		$this->db->delete("households");
	}

	public function checkmeterifExist($meter){
		return $this->db->query("SELECT meter_num FROM households WHERE meter_num = '$meter'")->result_array();
	}

	public function exportHousehold(){
		return $this->db->query("SELECT a.*,MAX(b.current_reading) as prev FROM households a 
		LEFT JOIN billed_household b ON a.id = b.household_id
		GROUP BY a.id")->result_array();
	}
}
?>