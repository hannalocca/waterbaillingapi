<!doctype html>
<html lang="en">
<head>

</head>
<body>

<div class="wrapper">
    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed" style="background-color: #4267b2;">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" >
                        <span class="sr-only" style="color: #fff;">Toggle navigation</span>
                        <span class="icon-bar" style="color: #fff;"></span>
                        <span class="icon-bar" style="color: #fff;"></span>
                        <span class="icon-bar" style="color: #fff;"></span>
                    </button>
                    <!-- <a class="navbar-brand" href="#" style="color: #fff;">Home</a> -->
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                    </ul>
                     <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="<?php echo base_url()?>logout" style="color: #fff;">
                                Log out
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">
                <div class="row">

                    <div class="container">
                        <div class="row">
                            <div class="timeline-left" >
                            <h3 style="color:red;">Billing</h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                                    <div class="panel panel-default" style="background-color: #eeeded;">
                                        <div class="panel-heading">Input</div>
                                        <div class="panel-body">
                                            <h3 style="color:red;">Compute Consumption</h3>
                                            <form class="form-horizontal form-label-right formcompute">
                                                <div class="col-md-8 col-sm-8 col-lg-8 col-xs-12">
                                                    <div class="form-group">
                                                    <label class="control-label col-md-6 col-sm-6 col-lg-6 col-xs-12">Select Household:</label>
                                                    <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                        <select class="form-control employeeselect" name="employee">
                                                        </select>
                                                    </div>
                                                    
                                                </div>
                                                <div class="form-group noofdays">
                                                    <label class="control-label col-md-6 col-sm-6 col-lg-6 col-xs-12">Previous Reading:</label>
                                                    <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                        <label class="control-label previous_reading">
                                                            0
                                                        </label>    
                                                    </div>
                                                </div>
                                                <div class="form-group noofdays">
                                                    <label class="control-label col-md-6 col-sm-6 col-lg-6 col-xs-12">Current Reading:</label>
                                                    <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                        <input type="number" class="form-control" id="current_reading" value="0" name="nodays">
                                                    </div>
                                                </div>
                                                <div class="form-group noofdays">
                                                    <label class="control-label col-md-6 col-sm-6 col-lg-6 col-xs-12">Total Consumption:</label>
                                                    <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                        <label class="control-label total_consumption">
                                                            0 @ 10.92
                                                        </label>  
                                                    </div>
                                                </div>
                                                <div class="form-group noofdays">
                                                    <label class="control-label col-md-6 col-sm-6 col-lg-6 col-xs-12">Total Cost of water:</label>
                                                    <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                        <label class="control-label total_water">
                                                            0.00
                                                        </label>  
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <button class="btn btn-sm btn-info pull-right" style="background-color: red;color:white;border:0;" type="submit">Compute</button>
                                                    
                                                </div>
                                            </form>     
                                                
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-lg-4 col-xs-12">
                                                    <div class="form-group" style="border:1px solid #eaeaea;padding: 3px;">
                                                        <input type="hidden" value="800.00" id="ratefromform" name="">
                                                        <input type="hidden" value="" id="perdayfromform" name="">
                                                        <input type="hidden" value="" id="minutesforundertime" name="">
                                                        <input type="hidden" value="" id="hourforundertime" name="">
                                                        <input type="hidden" value="" id="taxjud" name="">
                                                        <input type="hidden" value="" id="sssjud" name="">
                                                        <input type="hidden" value="" id="philjud" name="">
                                                        <input type="hidden" value="" id="pagjud" name="">
                                                        <input type="hidden" value="<?php echo $_GET['cutoff_id'];?>" id="cutoff_id" name="">
                                                        <input type="hidden" value="<?php echo $_GET['range'];?>" id="rangejus" name="">
                                                        <p class="pfullname control-label"><b>Fullname: ---</b></p>
                                                        <p class="pdepartment control-label"><b>Address:</b></p>
                                                        <p class="meter control-label"><b>Meter No.:</b></p>
                                                        <p class="stubout control-label"><b>Stub-out No.:</b></p>
                                                        
                                                    </div>
                                                </div>
                                            
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class= "row">
                                <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                                    <div class="panel panel-default" style="background-color: #eeeded;">
                                        <div class="panel-heading"><b>Anami North Water Bill August 2020</b></div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                        <div class="form-group noofdays">
                                                            <label class="control-label ">Name:</label>
                                                                <label class="control-label pfullname">
                                                                    0
                                                                </label>  
                                                        </div>
                                                        <div class="form-group noofdays">
                                                            <label class="control-label">Address:</label>
                                                                <label class="control-label pdepartment">
                                                                    0
                                                                </label>  
                                                        </div>
                                                        <div class="form-group noofdays">
                                                            <label class="control-label">Meter No:</label>
                                                                <label class="control-label meter">
                                                                    0
                                                                </label>  
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                    <div class="form-group noofdays">
                                                        <label class="control-label">Bill No:</label>
                                                                <label class="control-label ">
                                                                    00001
                                                                </label>  
                                                        </div>
                                                        <div class="form-group noofdays">
                                                            <label class="control-label">Bill Date:</label>
                                                                <label class="control-label ">
                                                                    August 08, 2020
                                                                </label>  
                                                        </div>
                                                        <div class="form-group noofdays">
                                                            <label class="control-label">Coverage:</label>
                                                                <label class="control-label ">
                                                                    08/07/2020 - 09/07/2020
                                                                </label>  
                                                        </div>
                                                    </div>
                                                </div>
                                                <table class="table table-bordered">
                                                <tr>
                                                    <th style="width:15%;"></th>
                                                    <th style="width:27.5%;">Service Period</th>
                                                    <th style="width:27.5%;">Meter Reading</th>
                                                    <th style="width:30%;">Due date</th>
                                                </tr>
                                                <tr>
                                                    <td>Present Reading</td>
                                                    <td></td>
                                                    <td class="current_meter"></td>
                                                    <td rowspan="3" style="text-align: center;"><b>August 20</b></td>
                                                </tr>
                                                <tr>
                                                    <td>Previous Reading</td>
                                                    <td></td>
                                                    <td class="previous_meter"></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" style="text-align:right;"><b>Total Water Consumed</b></td>
                                                    <td class="total_meter"></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3"></td>
                                                    <td rowspan="5">
                                                        <table class="table"> 
                                                            <tr>
                                                                <th></th>
                                                                <th>m3</th>
                                                                <th>rate/cubic</th>
                                                            </tr>
                                                            <tr>
                                                                <td>Current bill</td>
                                                                <td class="current_consumption"></td>
                                                                <td class="total_water"></td>
                                                            </tr>   
                                                            <tr>
                                                                <td>Previous bill</td>
                                                                <td> </td>
                                                                <td></td>
                                                            </tr>   
                                                            <tr>
                                                                <td></td>
                                                                <td><b>Total Due</b></td>
                                                                <td class="total_water"></td>
                                                            </tr>  
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3"></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3"></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3"></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3"></td>
                                                </tr>
                                                </table>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-1 col-xs-12 col-lg-1 col-sm-1">
                            
                        </div><div class="col-md-10 col-xs-12 col-lg-10 col-sm-10">
                            <h3 style="color:red;"><center>Payslips<center></h3><table class="table table-bordered tblpayrolled" style="background-color: #eeeded;">
                                <tr>
                                    <th>Firstname</th>
                                    <th>Middle</th>
                                    <th>Lastname</th>
                                    <th>Department</th>
                                    <th>Position</th>
                                    <th>Action</th>
                                </tr>
                                <tr>
                                    
                                </tr>
                            </table>
                        </div><div class="col-md-1 col-xs-12 col-lg-1 col-sm-1">
                           
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>



</body>
</html>
<script>
$(document).ready(function () {
    selectPayrolled();
    function selectPayrolled(){
        cutoff_id = $('#cutoff_id').val();
        $.ajax({
            url:'<?php echo base_url()?>Payroll/selectPayrolled',
            data:{cutoff_id:cutoff_id},
            dataType:'json',
            type:'post',
            success:function(data){
                $.each(data,function(key,val){
                    tr = '<tr><td>'+val.Firstname+'</td>'
                        +'<td>'+val.Middlename+'</td>'
                        +'<td>'+val.Lastname+'</td>'
                        +'<td>'+val.Department+'</td>'
                        +'<td>'+val.Position+'</td>'
                        +'<td><a data-toggle="tooltip" title="Print" href="<?php echo base_url()?>Payroll/PrintPayslipSpecific?payroll_id='+val.payroll_id+'" class="btn btn-md btn-success" style="background-color:#87CB16;border:0;color:white;"><i class="fa fa-print"></i> </a><button data-toggle="tooltip" title="Delete" data-id="'+val.payroll_id+'" style="background-color:red;color:white;border:0;" type="button" class="btn btn-md btn-danger deletenapayroll" data-fullname="'+val.Lastname+', '+val.Firstname+' '+val.Middlename+' '+val.Suffix+' ."><i class="fa fa-trash"></i> </button></td></tr>';
                        $('.tblpayrolled').append(tr);
                });
            },error:function(){
                alert("Opps! Server Error.");
            }
        });
    }
    $('.btnsavess').hide();
    $('.formcompute').bootstrapValidator({
        fields:{
            employee:{
                validators:{
                    notEmpty:{
                        message:'This field is required.'
                    }
                }
            },undertimehour:{
                validators:{
                    notEmpty:{
                        message:'This field is atleast 0 (if no Under time).'
                    }
                }
            },undertimeminute:{
                validators:{
                    notEmpty:{
                        message:'This field is atleast 0 (if no Under time).'
                    }
                }
            },iovertimehour:{
                validators:{
                    notEmpty:{
                        message:'This field is atleast 0 (if no Over time).'
                    }
                }
            },iovertimeminute:{
                validators:{
                    notEmpty:{
                        message:'This field is atleast 0 (if no Over time).'
                    }
                }
            },cashadvance:{
                validators:{
                    notEmpty:{
                        message:'This field is atleast 0 (if no Cash Advance)'
                    }
                }
            },absent:{
                validators:{
                    notEmpty:{
                        message:'This field is atleast 0 (if no Absent).'
                    }
                }
            },adjustments:{
                validators:{
                    notEmpty:{
                        message:'This field is atleast 0 (if no Adjustments).'
                    }
                }
            },nodays:{
                validators:{
                    notEmpty:{
                        message:'This field is atleast 0 (if no number of days).'
                    }
                }
            },
        }
    });
    $('.overtime').hide();
    if($('#chkovertime').is(':checked')){
            $('.overtime').show();
        }
        else{
            $('.overtime').hide();
        }
        if($('#chkundertime').is(':checked')){
            $('.undertime').show();
        }
        else{
            $('.undertime').hide();
        }
        if($('#adjustments').is(':checked')){
            $('.adjustments').show();
        }
        else{
            $('.adjustments').hide();
        }
        $('#adjustments').on('click',function(){
        if($(this).is(':checked')){
            $('.adjustments').show();
            $('.adjustments').val("0");
        }
        else{
            $('.adjustments').hide();
            $('.adjustments').val("0");
        }
        
    });
    $('#chkovertime').on('click',function(){
        if($(this).is(':checked')){
            $('.overtime').show();
            $('.otnoofhours').val("0");
            $('.otpercent').val("0");
        }
        else{
            $('.overtime').hide();
            $('.otnoofhours').val("0");
            $('.otpercent').val("0");
        }
        
    });$('#chkundertime').on('click',function(){
        if($(this).is(':checked')){
            $('.undertime').show();
        }
        else{
            $('.undertime').hide();
        }
        
    });
    $('.formcompute').on('submit',function(e){
        e.preventDefault();
        var previous_reading = 10000;
        var current_reading = $('#current_reading').val();
        var result = parseInt(current_reading) - parseInt(previous_reading);
        var total_cost = result * 10.92;

        $('.total_consumption').text(result +"          @    10.92");
        $('.total_water').text(total_cost.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
        $('.current_meter').text(current_reading);
        $('.previous_meter').text(previous_reading);
        $('.total_meter').text(result);
        $('.current_consumption').text(result +"          @    10.92");
    });
    $('.employeeselect').select2({
        placeholder:'<--Select-->'
    });
    loademployee();
    function loademployee(){
        $.ajax({
            url:'<?php echo base_url()?>Payroll/selectEmployeetoCompute/<?php echo $_GET["cutoff_id"]?>',
            dataType:'json',
            success:function(data){
                 option = $("<option></option>");
                    option.val("");
                    option.text("SELECT");
                    $('.employeeselect').append(option);
                $.each(data,function(key,val){
                    option = $("<option></option>");
                    option.val(val.employee_id);
                    option.text(val.fullname);
                    $('.employeeselect').append(option);
                });
            }
        });
    }
    $('.computeagain').on('click',function(){
        location.reload();
    });
    $('.employeeselect').on('change',function(){
        id = $(this).val();
        $.ajax({
            url:'<?php echo base_url()?>Payroll/selectSpecificEmployeetoCompute/'+id,
            dataType:'json',
            success:function(data){
                $.each(data,function(key,val){
                    $('.pfullname').text("Fullname : "+val.Lastname+", "+val.Firstname+" "+val.Middlename+" "+val.Suffix+" .").css({"font-weight":"bold"});
                    $('.pdepartment').text("Address: Phase 1, Lot 2").css({"font-weight":"bold"});
                    $('.meter').text("Meter No.: HP8144").css({"font-weight":"bold"});
                    $('.stubout').text("Stub-out No.: A0001").css({"font-weight":"bold"});
                    $('.previous_reading').text("10,000").css({"font-weight":"bold"});
                    $('.daccountnumber').text("Bank Account Number: "+val.Bank_account_number).css({"font-weight":"bold"});
                    $('.prateperdays').text(parseFloat(val.Rate_perday, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                    $('#perdayfromform').val(val.Rate_perday);
                    $('.allowance').text(parseFloat(val.Allowance/2, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                    $('.pallowance').text(parseFloat(val.Allowance/2, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                    $('.basicpay').text(parseFloat(val.Salary/2, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                      $('#taxjud').val(val.TAX);
                    $('#philjud').val(val.PHILHEALTH_amount);
                    $('#pagjud').val(val.PAGIBIG_amount);
                    $('#sssjud').val(val.SSS_amount);
                    if($('#rangejus').val()=="25"){
                       $('.ptax').text(parseFloat(val.TAX, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                    }else{
                        $('.psss').text(parseFloat(val.SSS_amount, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                        $('.pphilhealth').text(parseFloat(val.PHILHEALTH_amount, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                        $('.ppagibig').text(parseFloat(val.PAGIBIG_amount, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                    }
                    valsalaryforundertime = parseFloat(val.Salary/30);
                     $('.prateperday').text("Rate per day : "+parseFloat(valsalaryforundertime, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()).css({"font-weight":"bold"});
                    hourforundertime = valsalaryforundertime/parseFloat(8);
                    minutesforundertime = hourforundertime/60;
                    $('#hourforundertime').val(hourforundertime);
                    $('#minutesforundertime').val(minutesforundertime);
                });
                
            },error:function(){
                alert("Opps. Server error!");
            }
        });
    });

    $('.payrollperiod').on('change',function(){
        if($(this).val()=="Monthly"){
            $('.monthlyperiod').show();
            $('.noofdays').hide();
        }
        if($(this).val()=="Daily"){
            $('.monthlyperiod').hide();
            $('.noofdays').show();
        }

    });
    $('.payrollperiod').trigger('change');
    $('.savenajudangpayslip').on('click',function(){
       prateperdays = $('.prateperdays').text().replace(',','');
       pnumberofdays = $('.pnumberofdays').text().replace(',','');
       povertimeamount = $('.povertimeamount').text().replace(',','');
       pallowance = $('.pallowance').text().replace(',','');
       padjustments = $('.padjustments').text().replace(',','');
       pgrosspay = $('.pgrosspay').text().replace(',','');
       ptotalgrosspay = $('.ptotalgrosspay').text().replace(',','');

       absentamount = $('.absentamount').text().replace(',','');
       pundertimeamount = $('.pundertimeamount').text().replace(',','');
       pcashadvance = $('.pcashadvance').text().replace(',','');
       psss = $('.psss').text().replace(',','');
       pphilhealth = $('.pphilhealth').text().replace(',','');
       ppagibig = $('.ppagibig').text().replace(',','');
       ptax = $('.ptax').text().replace(',','');
       ptotaldeductions = $('.ptotaldeductions').text().replace(',','');
       pnetpay = $('.pnetpay').text().replace(',','');
       absentday = $('.iabsent').val();
       othour = $('.iovertimehour').val();
       otmin = $('.iovertimeminute').val();
       uthour = $('.iundertimehour').val();
       utmin = $('.iundertimeminute').val();


       employee_id = $('.employeeselect').val();
       cutoff_id = $('#cutoff_id').val();
       swal({
                                  title: "Are you sure to save the Payslip?",
                                  type: "info",
                                  showCancelButton: true,
                                  confirmButtonClass: "btn-primary",
                                  confirmButtonText: "Yes",
                                  cancelButtonText: "No",
                                  closeOnConfirm: false,
                                  closeOnCancel: false
                                },
                            function(isConfirm) {
                              if (isConfirm) {
                                swal("Saved!", "", "success"); 
                                $('.confirm').on('click', function(e){
                            $.ajax({
                            url:'<?php echo base_url()?>Payroll/savePayslip',
                            data:{
                                prateperdays:prateperdays,
                                pnumberofdays:pnumberofdays,
                                povertimeamount:povertimeamount,
                                pallowance:pallowance,
                                padjustments:padjustments,
                                pgrosspay:pgrosspay,
                                ptotalgrosspay:ptotalgrosspay,
                                absentamount:absentamount,
                                pundertimeamount:pundertimeamount,
                                pcashadvance:pcashadvance,
                                psss:psss,
                                pphilhealth:pphilhealth,
                                ppagibig:ppagibig,
                                ptax:ptax,
                                ptotaldeductions:ptotaldeductions,
                                pnetpay:pnetpay,
                                employee_id:employee_id,
                                cutoff_id:cutoff_id,
                                absentday:absentday,
                                othour:othour,
                                otmin:otmin,
                                uthour:uthour,
                                utmin:utmin
                            },
                            type:'post',
                            success:function(){
                                location.reload();
                            },error:function(){
                            }
                            });
                                });
              } else {
                swal("Cancelled!", "", "error");
              }
            });
       
    });
    $(document).on('click','.deletenapayroll',function(){
        id= $(this).data('id');
        fullname= $(this).data('fullname');
         swal({
                                  title: "Are you sure to delete "+fullname+" from payslips?",
                                  type: "info",
                                  showCancelButton: true,
                                  confirmButtonClass: "btn-primary",
                                  confirmButtonText: "Yes",
                                  cancelButtonText: "No",
                                  closeOnConfirm: false,
                                  closeOnCancel: false
                                },
                            function(isConfirm) {
                              if (isConfirm) {
                                swal("Deleted!", "", "success"); 
                                $('.confirm').on('click', function(e){
                            $.ajax({
                            url:'<?php echo base_url()?>Payroll/deleteFrompayslips',
                            data:{
                                id:id,
                            },
                            type:'post',
                            success:function(){
                                location.reload();
                            },error:function(){
                            }
                            });
                                });
              } else {
                swal("Cancelled!", "", "error");
              }
            });
    });
});

</script>
