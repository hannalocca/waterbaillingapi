<!doctype html>
<html lang="en">
<head>

</head>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/datatable/datatables.min.css"/>
 
    <script type="text/javascript" src="<?php echo base_url()?>assets/datatable/datatables.min.js"></script>
<div class="modal fade" id="confirm">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"  style="background-color: #eeeded;">
        <h5 class="modal-title"><h3>Create Cut-off</h3></h5>
      </div>
      <form class="form-horizontal label-left cutoffform" autocomplete="off" style="font-size: 17px;">
            <div class="modal-body"  style="background-color: #eeeded;">
                <center><p class="pcutoff" style="color:red;"><b>Cut-off is already exist.</b></p></center>
                <div class="col-xs-12 col-md-3 col-lg-3 col-sm-3">

                    <div class="form-group">
                        <label class="control-label">From:</label>
                        <select class="form-control month" name="from_" id="from_">
                            
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-md-2 col-lg-2 col-sm-2">
                    <div class="form-group">
                        <label class="control-label">Date:</label>
                        <select class="form-control" name="date_from" id="date_from">
                            <option value="11">11</option>
                            <option value="26">26</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-md-3 col-lg-3 col-sm-3">
                    <div class="form-group">
                        <label class="control-label">to:</label>
                        <select class="form-control month" name="to_" id="to_">
                            
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-md-2 col-lg-2 col-sm-2">
                    <div class="form-group">
                        <label class="control-label">Date:</label>
                        <select class="form-control" name="date_to" id="date_to">
                            <option value="10">10</option>
                            <option value="25">25</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-md-2 col-lg-2 col-sm-2">
                    <div class="form-group">
                        <label class="control-label">Year:</label>
                        <select class="form-control year" name="year" id="year">
                        </select>
                    </div>
                </div>
            </div> 
           <div class="modal-footer"  style="background-color: #eeeded;">
                <button type="button" class="btn btn-info btn-md pull-right" style="background-color: red;border: red;color: white;" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-info btn-md pull-right" style="background-color: #4267b2;border: #4267b2;color: white;">Save</button>
            </div>   
        </form> 
      </div>
    </div>
  </div>

<body>

<div class="wrapper">
    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed" style="background-color: #4267b2;">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" >
                        <span class="sr-only" style="color: #fff;">Toggle navigation</span>
                        <span class="icon-bar" style="color: #fff;"></span>
                        <span class="icon-bar" style="color: #fff;"></span>
                        <span class="icon-bar" style="color: #fff;"></span>
                    </button>
                    <!-- <a class="navbar-brand" href="#" style="color: #fff;">Home</a> -->
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                    </ul>
                     <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="<?php echo base_url()?>logout" style="color: #fff;">
                                Log out
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">
                <div class="row">

                    <div class="container">
                        <div class="row" >
                            <div class="timeline-left" style="width: 90%;">
                            <h3>List of Cut-off(s)</h3>
                            <a data-toggle="modal" data-toggle="tooltip" title="Create Cut-off" href="#" data-toggle="modal" data-target="#confirm" class="pull-right btn btn-md btn-info" style=";background-color:red;border:1px solid red;color:white;"><i class="fa fa-user-plus"></i></a>

                            <hr>
                            <br>
                            <table id="example" class="display">
                                <thead>
                                    <tr>
                                        <th>Cut-Off ID</th>
                                        <th>Cut-off</th>
                                        <th>Year</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($cutoff as $row){?>
                                        <tr>
                                            <td><?php echo $row->cutoff_id;?></td>
                                            <td><?php echo $row->from_." ".$row->date_from ." - ".$row->to_." ".$row->date_to;?></td>
                                            <td><?php echo $row->year;?></td>
                                            <td><a data-toggle="tooltip" title="Compute Payroll" href="<?php echo base_url()?>Payroll/computePayroll?cutoff_id=<?php echo $row->cutoff_id;?>&&range=<?php echo $row->date_to;?>" class="btn btn-md btn-success" style=";background-color:#4267b2;border:1px solid #4267b2;color:white;"><i class="fa fa-money fa-lg"></i></a>
                                                <a data-toggle="modal" data-toggle="tooltip" title="Delete" style="background-color: #FF4A55;color:white;" href="#" data-fullname="<?php echo $row->from_." ".$row->date_from ." - ".$row->to_." ".$row->date_to;?>" class="btn btn-danger btn-md delete" data-id="<?php echo $row->cutoff_id;?>"><i class="fa fa-trash"></i></a></td>
                                        </tr>
                                    <?php }?>
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>



</body>
</html>
<script>
$(document).ready(function () {
    $('#example').DataTable();
    cutofffields();
    $('.pcutoff').hide();
    $(document).on('click','.delete',function(){
        name = $(this).data('fullname');
        id = $(this).data('id');
        swal({
                                  title: "Are you sure to delete "+name+"?",
                                  type: "warning",
                                  showCancelButton: true,
                                  confirmButtonClass: "btn-primary",
                                  confirmButtonText: "Yes",
                                  cancelButtonText: "No",
                                  closeOnConfirm: false,
                                  closeOnCancel: false
                                },
                            function(isConfirm) {
                              if (isConfirm) {
                                swal("Deleted!", "", "success"); 
                                $('.confirm').on('click', function(e){
                                    $.ajax({
                                            url:'<?php echo base_url()?>Payroll/deleteCutfoff/'+id,
                                            success:function(data){
                                                window.location.href='<?php echo base_url()?>Payroll';
                                            }
                                        }); 
                                });
              } else {
                swal("Cancelled!", "", "error");
                $('.saving').removeAttr('disabled');
              }
            });
    });
    function cutofffields(){
        $('.month').find('option').remove();
        d = new Date();
        monthnow = d.getMonth();
        getyear = d.getFullYear();
        months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
        for(x=0;x<=11;x++){
            if(monthnow==x){
                option = $('<option selected="true"></option>');
                option.val(x);
                option.text(months[x]);
                $('.month').append(option);
            }else{
                option = $('<option></option>');
                option.val(x);
                option.text(months[x]);
                $('.month').append(option);
            }
            
        }
        year = parseInt(getyear);
        if(year==getyear){
                option = $('<option selected="true"></option>');
                option.val(year);
                option.text(year);
                $('.year').append(option);
            }
        for(x=1;x<=11;x++){
                option = $('<option></option>');
                option.val(year+x);
                option.text(year+x);
                $('.year').append(option);
        }
        
    }
    
    
    $('.cutoffform').on('submit',function(e){
        e.preventDefault();
        from_ = $('#from_').find('option:selected').text();
        date_from = $('#date_from').find('option:selected').text();
        to_ = $('#to_').find('option:selected').text();
        date_to = $('#date_to').val();
        year = $('#year').val();
        $.ajax({
            url:'<?php echo base_url()?>Payroll/selectExistCutoff/'+from_+'/'+date_from+'/'+to_+'/'+date_to+'/'+year,
            dataType:'json',
            success:function(data){
                if(data.result=="exist"){
                    $('.pcutoff').show();
                    setTimeout(function(){
                        $('.pcutoff').hide();
                    },2000);
                }
                else{
                    swal({
                                  title: "Are you sure to add this CUt off?",
                                  type: "info",
                                  showCancelButton: true,
                                  confirmButtonClass: "btn-primary",
                                  confirmButtonText: "Yes",
                                  cancelButtonText: "No",
                                  closeOnConfirm: false,
                                  closeOnCancel: false
                                },
                            function(isConfirm) {
                              if (isConfirm) {
                                swal("Saved!", "", "success"); 
                                $('.confirm').on('click', function(e){
                                    $.ajax({
                                            url:'<?php echo base_url()?>Payroll/addCutoff/'+from_+'/'+date_from+'/'+to_+'/'+date_to+'/'+year,
                                            success:function(data){
                                                window.location.href='<?php echo base_url()?>Payroll';
                                            },
                                            error:function(){
                                                swal({
                                                    title: 'Opps', 
                                                    text: 'Server Error!', 
                                                    type: "error",
                                                    timer: 1200,
                                                    showConfirmButton: false,
                                                })
                                            }
                                        }); 
                                });
              } else {
                swal("Cancelled!", "", "error");
                $('.saving').removeAttr('disabled');
              }
            });
                }
            },error:function(){
                swal({
                    title: 'Opps', 
                    text: 'Server Error!', 
                    type: "error",
                    timer: 1200,
                    showConfirmButton: false,
                })
            }
        });
        
        
    });
});
</script>
