<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body style="font-size: 13px;">
	<div class="container ">
		<h3><center>JaPhil Global Coins Corporation.</center></h3>
		<h4 class="headerss"><center>Account Numbers and Over all Total</center></h4>
		<h5 class="cutsum" ><center>Cut off:</center></h5>
		<table class="table table-bordered append" style="margin:30px;">
			<tr>
				<th>Firstname</th>
				<th>Middlename</th>
				<th>Lastname</th>
				<th>Account Number</th>
				<th>Amount</th>
			</tr>
			
		</table>
	</div>
	<input type="hidden" value="<?php echo $_GET['cutoff_id'];?>" id="cutoff_id" name="">
	<input type="hidden" value="<?php echo $_GET['category'];?>" id="cat" name="">
	<input type="hidden" value="<?php echo $_GET['branch'];?>" id="branch" name="">
	<input type="hidden" value="<?php $user = $this->session->userdata('session'); echo $user['Firstname']." ".$user['Middlename']." ".$user['Lastname'];?>" id="user" name="">
</body>
</html>
<script>
	$(document).ready(function(){
		cutoff_id = $('#cutoff_id').val();
		cat = $('#cat').val();
		branch = $('#branch').val();
		accountnumbers();
		function accountnumbers(){
			$.ajax({
				url:'<?php echo base_url()?>Payroll/accountnumbers',
				dataType:'json',
				type:'post',
				data:{cutoff_id:cutoff_id,cat:cat,branch:branch},
				success:function(data){
					var total = 0;
					$.each(data,function(key,val){
						$('.headerss').text("Payroll Summary for "+val.Department+"(s) - "+val.branch_name+" branch").css({"text-align":"center"});
						$('.cutsum').text("Cut-off: "+val.from_+" "+val.date_from+" - "+val.to_+" "+val.date_to+" , "+val.year).css({"text-align":"center","font-weight":"bold"});
						tr = '<tr>'
								+'<td>'+val.Firstname+'</td>'
								+'<td>'+val.Middlename+'</td>'
								+'<td>'+val.Lastname+'</td>'
								+'<td>'+val.Bank_account_number+'</td>'
								+'<td>'+parseFloat(val.pnetpay, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</td>'
							+'</tr>';
							total = parseFloat(total) + parseFloat(val.pnetpay);
							$('.append').append(tr);
					});
					trs = '<tr>'
								+'<td></td>'
								+'<td></td>'
								+'<td></td>'
								+'<td>Total :</td>'
								+'<td>'+parseFloat(total, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</td>'
							+'</tr>';
					$('.append').append(trs);
					window.print();
				}

			});
		}
	});
	
</script>