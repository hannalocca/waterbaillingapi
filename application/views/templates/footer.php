<!--   Core JS Files   -->
    <script src="<?php echo base_url()?>assets/js/jquery-3.1.1.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/js/bootstrap.min.js" type="text/javascript"></script>

    <!--  Checkbox, Radio & Switch Plugins -->
    <script src="<?php echo base_url()?>assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Charts Plugin -->
    <script src="<?php echo base_url()?>assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="<?php echo base_url()?>assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/datatable/datatables.min.css"/>
 
    <script type="text/javascript" src="<?php echo base_url()?>assets/datatable/datatables.min.js"></script>
    <script src="<?php echo base_url()?>assets/sweet-alert/sweetalert.min.js"></script>
    <script src="<?php echo base_url()?>assets/validator/bootstrapValidator.js"></script>
    <script src="<?php echo base_url()?>assets/validator/bootstrapValidator.min.js"></script>
    <script src="<?php echo base_url()?>assets/select2-4.0.6-rc.1/dist/js/select2.js"></script>
    <script src="<?php echo base_url()?>assets/select2-4.0.6-rc.1/dist/js/select2.min.js"></script>
   <script src="<?php echo base_url()?>assets/jquery.maskMoney.min.js"></script>
