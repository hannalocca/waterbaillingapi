<meta charset="utf-8" />
    <!-- <link rel="icon" type="image/png" href="<?php echo base_url()?>assets/img/favicon.ico"> -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url()?>assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="<?php echo base_url()?>assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="<?php echo base_url()?>assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>

    <!--     Fonts and icons     -->
    <link href="<?php echo base_url()?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url()?>assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

    <!-- Timeline CSS -->
    <link href="<?php echo base_url()?>assets/css/timeline.css" rel="stylesheet" />

    <!-- Custome CSS -->
    <link href="<?php echo base_url()?>assets/css/custom.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/sweet-alert/sweetalert.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/validator/bootstrapValidator.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/validator/bootstrapValidator.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/select2-4.0.6-rc.1/dist/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/select2-4.0.6-rc.1/dist/css/select2.css">
    
    