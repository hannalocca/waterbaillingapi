<?php 
class Settings extends CI_Controller
{
	function __construct(){
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, X-Requested-With");
		parent::__construct();
		$this->load->model('settingsModel');
		
	}
	public function save(){
		$data = $this->input->post();
		$this->settingsModel->save($data);
	}

	public function getAll(){
		$lists = $this->settingsModel->getAll();
		echo json_encode($lists);
	}

	public function delete(){
		$id = $this->input->post('id');
		$lists = $this->settingsModel->delete($id);
	}

	public function getLists(){
		$lists = $this->settingsModel->getLists();
		echo json_encode($lists);
	}
	
	public function login() {
		$data = $this->input->post();
		$result = $this->settingsModel->login($data['user'], $data['password']);
		echo json_encode($result);
	}
}
?>