<?php 
class Household extends CI_Controller
{
	function __construct(){
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, X-Requested-With");
		parent::__construct();
		$this->load->model('householdModel');
		
	}
	public function save(){
		$data = $this->input->post();
		$meter = $this->householdModel->checkmeterifExist($data['meter_num']);
		if (count($meter) > 0){
			if ($data['meterflg'] == '1'){
				echo json_encode(array('msg' => "Meter no. is already taken", 'color' => 'red'));
			} else {
				$this->householdModel->save($data);
				echo json_encode(array('msg' => "Successfully", 'color' => 'green'));
			}
		} else {
			$this->householdModel->save($data);
			echo json_encode(array('msg' => "Successfully", 'color' => 'green'));
		}
	}

	public function getAll(){
		$lists = $this->householdModel->getAll();
		echo json_encode($lists);
	}

	public function get(){
		$id = $this->input->post('id');
		$lists = $this->householdModel->get($id);
		echo json_encode($lists);
	}

	public function delete(){
		$id = $this->input->post('id');
		$lists = $this->householdModel->delete($id);
	}

	public function exportHousehold(){
		$lists = $this->householdModel->exportHousehold();
		echo json_encode($lists);
	}
}
?>