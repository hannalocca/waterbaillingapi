<?php 
class Billing extends CI_Controller
{
	function __construct(){
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, X-Requested-With");
		parent::__construct();
		$this->load->model('billingModel');
		
	}
	public function save(){
		$data = $this->input->post();
		$this->billingModel->save($data);
	}

	public function saveBilled(){
		$data = $this->input->post();
		
		foreach(json_decode($data['data']) as $val){
			$consumption = ($val->current - $val->previous) + $data['systemLossRate'];
			
			$amount_due = $consumption * $data['rateCubic'];
			$this->billingModel->saveBilled($val->id, $val->current, $val->previous,$amount_due, $data['bill_id']);
		 }
		 $this->billingModel->updateSystemloss($data['bill_id'],$data['systemLoss'],$data['systemLossRate'],$data['noOfHousehold']);
	}

	public function getAll(){
		$lists = $this->billingModel->getAll();
		echo json_encode($lists);
	}

	public function get(){
		$id = $this->input->post('id');
		$lists = $this->billingModel->get($id);
		$expense_name = explode(',', $lists[0]['expense_name']);
		$amount = explode(',', $lists[0]['amount']);
		$costs = array();
		for($var = 0;$var<count($expense_name); $var++){
			array_push($costs,array("expense_name" => $expense_name[$var],"amount" => $amount[$var]));
		}	
		$lists[0]['costs'] = $costs;
		echo json_encode($lists);
	}

	public function delete(){
		$id = $this->input->post('id');
		$lists = $this->billingModel->delete($id);
	}

	public function getAllBillbyHousehold(){
		$id = $this->input->post('id');
		$lists = $this->billingModel->getAllBillbyHousehold($id);
		echo json_encode($lists);
	}

	public function getHouseholdBill(){
		$id = $this->input->post('id');
		$result = $this->billingModel->getHouseholdBill($id);
		$expense_name = explode(',', $result[0]['expense_name']);
		$amount = explode(',', $result[0]['amount']);
		$costs = array();
		for($var = 0;$var<count($expense_name); $var++){
			array_push($costs,array("expense_name" => $expense_name[$var],"amount" => $amount[$var]));
		}	
		$result[0]['costs'] = $costs;
		echo json_encode($result);
	}

	public function updateHouseholdBill(){
		$data = $this->input->post();
		$data['month'] = date('n');
		$data['year'] = date("Y");
		$data['day'] = date("d");
		$result = $this->billingModel->updateHouseholdBill($data);
	}

	public function getPaidbillbyhousehold(){
		$id = $this->input->post('id');
		$result = $this->billingModel->getPaidbillbyhousehold($id);
		echo json_encode($result);
	}

	public function getUnpaidbillbyhousehold(){
		$id = $this->input->post('id');
		$result = $this->billingModel->getUnpaidbillbyhousehold($id);
		echo json_encode($result);
	}

	public function getBillReport(){
		$id = $this->input->post('id');
		$lists = $this->billingModel->getBillReport($id);
		echo json_encode($lists);
	}

	public function reportPaidStatusHousehold(){
		$id = $this->input->post();
		$lists = $this->billingModel->reportPaidStatusHousehold($id['bill_id'],$id['stats']);
		echo json_encode($lists);
	}

	public function reportCollectionBymonth(){
		$id = $this->input->post();
		if ($id['incomeType'] == 3){
			$lists = $this->billingModel->reportCollectionBymonth($id['bill_id'],$id['month'],$id['year']);
		} else {
			$lists = $this->billingModel->reportCollectionByDay($id['bill_id'], $id['month'],$id['year'], $id['day']);
		}
		echo json_encode($lists);
	}
	

}
?>